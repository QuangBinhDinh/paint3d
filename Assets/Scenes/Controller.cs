﻿using PaintIn3D.Examples;
using UnityEngine;
using UnityEngine.UI;
using PaintIn3D;
using TMPro;

public class Controller : MonoBehaviour
{
    public enum PaintState
    {
        Idle,Rotate,Dirt,Wash
    }

    public enum PaintPart
    {
        CarBody, CarWindow
    }

    [SerializeField]

    public P3dDragPitchYaw m_drag;

    public GameObject m_dirt;

    public GameObject m_wash, m_washWindow;

    public Image[] m_buttons;

    public Image m_switchButton;

    [HideInInspector]
    public PaintState m_state = PaintState.Idle;

    [HideInInspector]
    public PaintPart m_part = PaintPart.CarBody;

    [SerializeField]
    public GameObject m_body, m_window;

    [SerializeField]
    public TextMeshProUGUI m_text;

    

   void Awake()
    {
        m_buttons[0].color = Color.white;
        m_buttons[1].color = Color.white;
        m_buttons[2].color = Color.white;
        m_drag.enabled = false;
        m_dirt.SetActive(false);
        m_wash.SetActive(false);
        m_washWindow.SetActive(false);
        m_window.GetComponent<P3dPaintable>().enabled = false;
         
    }

    public void SetPaint(int i)
    {
        switch (i)
        {
            case 1:
                if (m_state == PaintState.Rotate)
                {
                    m_state = PaintState.Idle;
                    m_buttons[0].color = Color.white;
                    m_drag.enabled = false;
                }
                else
                {
                    m_state = PaintState.Rotate;
                    m_buttons[0].color = Color.gray;
                    m_buttons[1].color = Color.white;
                    m_buttons[2].color = Color.white;

                    m_drag.enabled = true;
                    m_dirt.SetActive(false);
                    m_wash.SetActive(false);
                    m_washWindow.SetActive(false);

                    
                }
                break;
            case 2:
                if (m_state == PaintState.Dirt)
                {
                    m_state = PaintState.Idle;
                    m_buttons[1].color = Color.white;
                    m_dirt.SetActive(false);
                }
                else
                {
                    m_state = PaintState.Dirt;
                    m_buttons[0].color = Color.white;
                    m_buttons[1].color = Color.gray;
                    m_buttons[2].color = Color.white;

                    m_drag.enabled = false;
                    m_dirt.SetActive(true);
                    m_wash.SetActive(false);
                    m_washWindow.SetActive(false);

                    m_dirt.transform.localPosition = new Vector3(1.5f, -0, -4f);
                }
                break;
            case 3:
                if (m_state == PaintState.Wash)
                {
                    m_state = PaintState.Idle;
                    m_buttons[2].color = Color.white;
                    m_wash.SetActive(false);
                    m_washWindow.SetActive(false);
                }
                else
                {
                    m_state = PaintState.Wash;
                    m_buttons[0].color = Color.white;
                    m_buttons[1].color = Color.white;
                    m_buttons[2].color = Color.gray;

                    m_drag.enabled = false;
                    m_dirt.SetActive(false);

                    
                    if (m_part == PaintPart.CarBody)
                    {
                        m_wash.SetActive(true);m_washWindow.SetActive(false);
                        m_wash.transform.localPosition = new Vector3(1.5f, -0, -4f);
                    }
                    else
                    {
                        m_washWindow.SetActive(false);m_washWindow.SetActive(true);
                        m_wash.transform.localPosition = new Vector3(1.5f, -0, -4f);
                    }
                    

                }
                break;
            default:
                break;
        }
    }

    public void SwitchPart()
    {
        if (m_part == PaintPart.CarBody)
        {
            m_switchButton.color = Color.gray;
            m_part = PaintPart.CarWindow;
            m_body.GetComponent<P3dPaintable>().enabled = false;
           
            m_window.GetComponent<P3dPaintable>().enabled = true;
           ;
            m_text.text = "Window part";
            if(m_state == PaintState.Wash)
            {
                m_wash.SetActive(false); m_washWindow.SetActive(true);
            }
        }
        else
        {
            m_switchButton.color = Color.white;
            m_part = PaintPart.CarBody;
            m_body.GetComponent<P3dPaintable>().enabled = true;
           
            m_window.GetComponent<P3dPaintable>().enabled = false;
            
            m_text.text = "Body part";
            if (m_state == PaintState.Wash)
            {
                m_wash.SetActive(true); m_washWindow.SetActive(false);
            }
        }
    }
    
}
