﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SprayMovement : MonoBehaviour
{
    
     [SerializeField] Transform m_camPivot;
    [SerializeField] Camera m_cam;

    [SerializeField] float m_radiusImpact;

    [SerializeField] float m_rockSpeed;

    [SerializeField] Controller m_control;

     ParticleSystem m_particle;

    Ray ray;
    RaycastHit hit;

    RaycastHit[] hitList;

    List<RaycastHit> hitRock;

    string m_tagBody = "CarBody";
    string m_tagWindow = "CarWindow";
    string m_tagRock = "Rock";

    Vector3 hitPoint;

    Vector3 startWPos, endWPos;

    Vector3 startLPos, endLPos;

    Vector3 distLocal;

    GameObject tempRock;
    Rigidbody rb;
        
    private void Awake()
    {
        
        m_particle = GetComponent<ParticleSystem>();
        
        
    }
    void Update() 
    {
        if (Input.GetMouseButtonDown(0))
        {
            startWPos = m_cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2));
            startLPos = m_camPivot.InverseTransformPoint(startWPos);
            
            LookAtHit();
        }
        else if (Input.GetMouseButton(0))
        {
            LookAtHit();
            endWPos = m_cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x , Input.mousePosition.y, 2));
            endLPos = m_camPivot.InverseTransformPoint(endWPos);


            distLocal = endLPos - startLPos;
            Debug.Log(distLocal);
            transform.localPosition +=distLocal;
            startLPos = endLPos;
        }
        
    }

    void LookAtHit()
    {
        ray = new Ray(transform.position, transform.forward);
        
        if(Physics.Raycast(ray,out hit, Mathf.Infinity))
        {
            if(hit.transform.CompareTag(m_tagBody) || hit.transform.CompareTag(m_tagWindow))
            {
                hitPoint = hit.point;
                
                if (m_control.m_state == Controller.PaintState.Dirt) return;

                Vector3 direct = hit.point - transform.position;
                hitList = Physics.SphereCastAll(transform.position, m_radiusImpact, direct, Vector3.Magnitude(direct));
                hitRock = hitList.Where(i => i.transform.CompareTag(m_tagRock)).ToList();
                AddForceToEachRock(hitPoint);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(transform.position, hit.point);
        Gizmos.DrawWireSphere(hitPoint, m_radiusImpact);
    }

    void AddForceToEachRock(Vector3 centerSphere)
    {
        if(hitRock.Count > 0)
        {
            foreach(RaycastHit hit in hitRock)
            {
                tempRock = hit.transform.gameObject;
                Vector3 directForce = tempRock.transform.position - centerSphere;
                rb = tempRock.GetComponent<Rigidbody>();
                rb.velocity = directForce.normalized * m_rockSpeed;
            }
        }
    }
}
